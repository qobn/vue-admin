import Vue from 'vue'
import Router from 'vue-router'
// import Home from '@/components/Home'
import User from '../pages/user/index.vue'
Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    {
      path: '/',
      name: 'Main',
      // component: Home //直接引入
      redirect: '/home',
      component: ()=>import('../pages/Main') ,//按需引入 （按需引入的话上边那个import就可以不要了）
      children:[
        {
          path:'/home',
          name:'home',
          component:()=>import('../pages/home')
        },
        {
          path:'/user',
          name:'user',
          component:()=>import('../pages/user')
        },
        {
          path:'/mall',
          name:'mall',
          component:()=>import('../pages/mall')
        },
        {
          path:'/pageOne',
          name:'pageOne',
          component:()=>import('../pages/other/pageOne.vue')
        },
        {
          path:'/pageTwo',
          name:'pageTwo',
          component:()=>import('../pages/other/pageTwo.vue')
        },
      ]
    },
    
  ]
})
