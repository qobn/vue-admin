// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router'
import store from './store'
import http from 'axios'
import * as echarts from 'echarts'

Vue.config.productionTip = false
Vue.prototype.$http=http
Vue.prototype.$echarts = echarts;

Vue.use(ElementUI);
Vue.use(echarts);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
