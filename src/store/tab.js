export default {
  state: {
    isCollapse: false,
    tabsList: [
      {
        path: "/",
        name: "home",
        lable: "首页",
        icon: "home",
        choose: true
      }
    ],
  },
  mutations: {
    collapseMenu(state) {
      state.isCollapse = !state.isCollapse;
    },
    selectMenu(state, val) {
      state.tabsList.forEach(item => {
        item.choose = false;
      });
      val.choose = true;
      const result = state.tabsList.findIndex(item => item.name == val.name);
      if (result == -1) {
        state.tabsList.push(val);
      }else{
        state.tabsList.forEach(item =>{
            if (item.name==val.name) {
                item.choose=val.choose;
            }
        })
      }
    },
    closeTag(state,val){
        console.log(val)
        //当移除的是选中的item时，需要做位移
        if(val.choose){
            const oldSize = state.tabsList.length-1
            const result = state.tabsList.findIndex(item => item.name == val.name);
            state.tabsList.splice(result,1);
            if(result==oldSize){
                state.tabsList[state.tabsList.length-1].choose=true
            }else{
                state.tabsList[result].choose=true
            }
        }else{
            const result = state.tabsList.findIndex(item => item.name == val.name);
            state.tabsList.splice(result,1);
        }
    }
  }
};
