import envurl from '../config/envurl.js'
var currentEnv = envurl.currentEnv
class EnvUtils{
    getBaseUrl(){
        if(currentEnv==0){//测试
            return envurl.debug.env.url
        }else if(currentEnv==1){//正式
            return envurl.release.env.url
        }
    }
    getLoginUrl(){
        if(currentEnv==0){//测试
            return envurl.debug.env.login
        }else if(currentEnv==1){//正式
            return envurl.release.env.login
        }
    }
}
export default EnvUtils;    