import envurl from "../config/envurl";
const currentEnv = envurl.currentEnv;
export const getTime=(param)=>{
  return Date.now();
}
export const log = (param)=>{
  if (currentEnv == 0) {
    Console.log(param);
  }
}
export const isEmpty = (param)=>{
  if(param == ''||param==null||param==undefined){
    return true;
  }else{
    return false;
  }
}