import axios from 'axios';
import envurl from '../config/envurl'
import EnvUtils from '../env/envUtils.js'
let envObj = new EnvUtils();
let baseUrl = envObj.getBaseUrl()
class HttpRequest{
    constructor(baseUrl){
        this.baseUrl = baseUrl
    }
    // axios 相关配置
    getInsideConfig(){
        const config = {
            baseUrl: baseUrl,
            headers:{
            }
        }
        return config
    }
    interceptors(instance){
        // 添加拦截器
        instance.interceptors.request.use(function(config){
            //发送请求之前做些什么
            config.headers["authorization"]="18510255744"     
            return config
        },function(err){
            //请求异常处理
            return Promise.reject(err)
        });
        instance.interceptors.response.use(function(response){
            //响应数据做处理
            return response
        },function(err){
            //响应异常处理
            return Promise.reject(err)
        })
    }

    request(options){
        const instance = axios.create()
        options = Object.assign(this.getInsideConfig(),options)
        this.interceptors(instance)
        return instance(options)
    }
}
export default new HttpRequest(baseUrl)