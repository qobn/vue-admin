import axios from './axios';
import envurl from '../config/envurl'
import EnvUtils from '../env/envUtils'
//多域名请求用上边这个 另外多域名情况下最好配置多个axios，多个data.js 这样互不干扰  用这个拦截器依然会走 没问题
// let envObj = new EnvUtils();
// let baseUrl = envObj.getBaseUrl()
// console.log(baseUrl)
// let loginApi = envObj.getLoginUrl()
//单域名请求用下边这个
let baseUrl = axios.baseUrl 
export const phoneQuery = (param) =>{
    let url = baseUrl+'admin/Query/phoneQuery'
    console.log(url)
    return axios.request({
        url: url,
        method: 'POST',
        data: param
    })
}

export const getSellNum = (param)=>{
    let url = baseUrl+'admin/Query/getSellNum'
    return axios.request({
        url: url,
        method: 'POST',
        data: param
    })
}

export const getSellCircle = (param)=>{
    let url = baseUrl+'admin/Query/getSellCircle'
    return axios.request({
        url: url,
        method: 'POST',
        data: param
    })
}
export const queryUsers=(param)=>{
    let url = baseUrl+'admin/Query/queryUsers'
    return axios.request({
        url: url,
        method: 'POST',
        data: param
    })
}
export const addUser=(param)=>{
    let url = baseUrl+'admin/Query/addUser'
    return axios.request({
        url: url,
        method: 'POST',
        data: param
    })
}
export const updateUser=(param)=>{
    let url = baseUrl+'admin/Query/updateUser'
    return axios.request({
        url: url,
        method: 'GET',
        data: param
    })
}
