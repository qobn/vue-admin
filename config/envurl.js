//如果多个baseUrl 拦截器只能有一个设置，这样的话 ，需要多个拦截器实现 
export default {
  debug: {
    env: {
      url: "http://www.joke.io/index.php/",
      login: "http://www.joke.io/index.php/"
    }
  },
  release: {
    env: {
      url: "http://joke.eatandshow.com/index.php/",
      login: "http://joke.eatandshow.com/index.php/"
    }
  },
  currentEnv: 1 // 0 debug 1 正式
}